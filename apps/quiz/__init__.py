import pathlib
from framework import app, system, geom, widget


QUESTION = 'Who developed Sketchpad in 1963?'

POSSIBILITIES = (
    'Alan Turing',
    'Ivan Sutherland',
    'Donald Knuth',
    'Douglas Engelbart'
)

CORRECT_INDEX = 1


class QuizApp(app.App):
    def __init__(self):
        super().__init__()
        system.query_positions_from_user(
            'Please select where to place the quiz. Point where and say "here"',
            'here',
            success_callback=self._on_pos_selected,
            cancel_callback=self._on_cancel
        )

    def _on_pos_selected(self, positions):
        pos = positions[0]
        QuizMainView(self, pos)

    def _on_cancel(self):
        self.close()


class QuizMainView(app.View):
    def __init__(self, app, pos):
        super().__init__(app)
        area = geom.rect(pos, geom.Vec2(10, 40))
        if not self.request_space(area):
            system.speak('Space already occupied! Closing app.')
            app.close()
            return
        for i, choice in enumerate(POSSIBILITIES):
            self.container.add(ChoiceButton(
                geom.rect(geom.Vec2(0, 10*i) + pos, geom.Vec2(10, 10)),
                choice,
                i == CORRECT_INDEX
            ))
        system.speak(QUESTION)


class ChoiceButton(widget.Widget):
    def __init__(self, rect, text, is_correct):
        super().__init__(rect)
        self.text = text
        self.is_correct = is_correct
        _, shapes = geom.import_svg(relative_path('button.svg'), width=8)
        transform = geom.Affine.translation(rect.center - geom.Vec2(4, 4))
        self.shapes = [s * transform for s in shapes]
        system.listen_for_command('select', self, self._on_select)

    def render(self):
        return self.shapes

    def hover_enter(self, pos):
        system.speak(self.text)

    def _on_select(self, _, __):
        system.speak(self.text + ' is ' + ('correct' if self.is_correct else 'incorrect'))
        return True


def relative_path(path):
    return str(pathlib.Path(__file__).resolve().parent / path)
