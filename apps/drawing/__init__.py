"""Implements a simple drawing application"""
import itertools
import math
import os.path

from framework import widget, geom, properties, app, system


class DrawingApp(app.App):
    def __init__(self):
        system.query_positions_from_user(
            'Please select a rectangle to draw on. Point out the two corners and say here',
            'here',
            num_positions=2,
            success_callback=self._start_canvas_view,
            cancel_callback=self.close)

    def _start_canvas_view(self, positions):
        rect = geom.BoundingBox.from_points(positions)
        DrawingCanvasView(self, rect)


class DrawingCanvasView(app.View):
    def __init__(self, app, rect):
        super().__init__(app)
        if not self.request_space(rect):
            system.speak('Selected rectangle is not free. Closing drawing app')
            app.close()
            return
        self._canvas_rect = rect
        system.listen_for_command('line', self, self._on_line)
        system.listen_for_command('rectangle', self, self._on_rect)
        system.listen_for_command('circle', self, self._on_circle)
        system.listen_for_command('print', None, self._on_print)
        system.listen_for_command('extend', None, self._on_extend)
        self._unprinted_shapes = []
        system.speak('Use the following drawing commands: Line, rectangle, circle')

    def _on_line(self, positions, _):
        if len(positions) != 2:
            system.speak('Please select two points')
            return True
        system.speak('Drawing line')
        self._unprinted_shapes.append(geom.Polyline(positions))
        return True

    def _on_rect(self, positions, _):
        if len(positions) != 2:
            system.speak('Please select two points')
            return True
        system.speak('Drawing rectangle')
        box = geom.BoundingBox(positions)
        self._unprinted_shapes.append(box)
        return True

    def _on_circle(self, positions, _):
        if len(positions) != 2:
            system.speak('Please select two points')
            return True
        system.speak('Drawing circle')
        p1, p2 = positions
        center = 0.5*(p1 + p2)
        radius = 0.5 * p1.distance_to(p2)
        circle = geom.circle(center, radius)
        self._unprinted_shapes.append(circle)
        return True

    def _on_print(self, _, __):
        system.speak('Printing')
        for shape in self._unprinted_shapes:
            self.container.add(widget.StaticWidget(shape.bounding_box, [shape]))
        self._unprinted_shapes.clear()
        return True

    def _on_extend(self, _, positions_over_unclaimed):
        if not positions_over_unclaimed:
            system.speak('Please select where to extend to')
        new_rect = geom.BoundingBox.from_points([
            self._canvas_rect.min_point,
            self._canvas_rect.max_point
        ] + positions_over_unclaimed)
        if not self.request_space(new_rect):
            system.speak('Some space already blocked')
        else:
            system.speak('Canvas extended')
            self._canvas_rect = new_rect
        return True
