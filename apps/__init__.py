"""Central place to register apps to the system"""
from framework import system
from . import drawing, quiz

system.register_app(drawing.DrawingApp, 'Drawing')
system.register_app(quiz.QuizApp, 'Quiz')
