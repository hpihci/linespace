# Linespace
A 3D plotter for the blind

## Setup

### Dependencies

Optional: Use [virtualenv](https://virtualenv.pypa.io) to create an isolated Python environment

#### Windows
Install Python >= 3.4 (or make sure `pip` is installed). You can follow the steps in the [documentation](https://docs.python.org/3/using/windows.html). Add the installation directory to the `PATH`, to [run Python conveniently from a command prompt](https://docs.python.org/3/using/windows.html#configuring-python).

The `planar` package has native C extensions. To avoid needing a compiler, download the appropriate [python wheel](http://pythonwheels.com/) file from [here](http://www.lfd.uci.edu/~gohlke/pythonlibs/#planar) depending on system architecture and python version.
Then run
```
pip install planar‑0.4‑cp3X‑none‑winXX.whl
```

#### Linux and OSX
Python should be available by default. If not, see the [Python documentation](https://docs.python.org/3/using/index.html).
Depending on your setup, you might need to substitute `pip` by `pip3` below.

#### For all systems
```
pip install -r requirements_dev.txt
```

### Linespace
Simply clone this repository using git (see top-right of this page for the url).


## Run
`python3 start_dev.py` (or simply `./start_dev.py` if you are on Linux or OSX)