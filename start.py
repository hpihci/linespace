#!/usr/bin/env python3
import logging

from framework import system
from framework.plugins import debugger
from framework.plugins.output import printer
from framework.plugins.input import alvar


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    system.run((
        debugger.DebugServer(),
        alvar.Plugin(),
        printer.PrinterHandler(),
    ))
