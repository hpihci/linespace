"""Implements loading of a simple configuration file format"""
import json


class _JsonProperties:
    """Provides convenient access to config values

    Nested config values can be accessed by using dotted.paths
    in the index operator (eg config['path.to.variable']).
    """
    def __init__(self, path):
        with open(path) as file:
            self._data = json.load(file)

    def __getitem__(self, path):
        obj = self._data
        for pathpart in path.split('.'):
            obj = obj[pathpart]
        return obj


def load(path):
    """Loads a configuration file"""
    return _JsonProperties(path)


# pylint: disable=invalid-name
settings = load('./settings.json')
