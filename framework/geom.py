"""
Module for everything geometry related.

Uses the planar module internally, but provides all important classes,
so no extra import is needed.
"""
import math
import warnings
import xml.etree.ElementTree as etree

import planar
# Import extra classes to provide them as exports
# pylint: disable=unused-import
from planar import Vec2, Polygon, Affine, BoundingBox
from planar.line import LineSegment


class Polyline(planar.Seq2):
    """Class for polyline shapes."""
    @property
    def bounding_box(self):
        """Returns the bounding box around the polyline."""
        return BoundingBox.from_points(self)


def circle(center, radius, max_line_length=1):
    """Creates a polygon to approximate a circle.

    :param Vec2 center:
        Center of the circle.
    :param radius:
        Radius of the circle.
    :param max_line_length:
        Maximum length of the line segments of the resulting polygon.
    """
    outline_length = 2 * math.pi * radius
    num_lines = int(math.ceil(outline_length / max_line_length))
    return Polygon.regular(num_lines, radius, center)


class AllArea:
    """Special area object which includes everything"""
    # pylint: disable=no-self-use
    def contains_point(self, _):
        """Same method as in Polygon or BoundingBox"""
        return True


class NullArea:
    """Area object that doesn't contain any points."""

    # pylint: disable=no-self-use
    def contains_point(self, _):
        """Same method as in Polygon or BoundingBox.

        Always returns False.
        """
        return False


def rect(pos, size):
    """Helper to construct a bounding box.

    :param planar.Vec2 pos: Top-left position.
    :param planar.Vec2 size: Size.
    :rtype: BoundingBox
    """
    return BoundingBox((
        pos,
        pos + planar.Vec2(size.x, 0),
        pos + size,
        pos + planar.Vec2(0, size.y)
    ))


def bbox_project(bbox, point):
    """Project a point onto the outline of a bounding box."""
    # Not the most efficient implementation, could be easily optimized
    assert isinstance(bbox, BoundingBox)
    assert isinstance(point, Vec2)
    lines = (
        LineSegment(bbox.min_point, Vec2(bbox.width, 0)),
        LineSegment(bbox.max_point, Vec2(0, -bbox.height)),
        LineSegment(bbox.max_point, Vec2(-bbox.width, 0)),
        LineSegment(bbox.min_point, Vec2(0, bbox.height))
    )
    points = (l.project(point) for l in lines)
    return min(points, key=lambda p: p.distance_to(point))


_SVG_TAGS = {
    '{http://www.w3.org/2000/svg}polygon': Polygon,
    '{http://www.w3.org/2000/svg}polyline': Polyline
}
_SVG_IGNORE_TAGS = {
    '{http://www.w3.org/2000/svg}defs'
}


def import_svg(path, width=None, height=None):
    """Imports an svg to a list of polylines and polygons.

    Warning: Only very simple svgs can be imported!
             Only toplevel <polyline> and <polygon> tags are supported
             (all others are ignored and a warning is issued).
             Transforms and non default units are also not supported.
             See util/svg_simplify for a tool to convert an svg to this
             simpler format. Also, the size attribute of the svg must
             match the desired area of the svg to be imported.

    The returned shapes are scaled according to the width and height
    parameters, and are moved to (0,0).

    :param str path: Path to the svg
    :param float | None width: The width to which to scale the imported
                               lines. If None, it is calculated by the
                               aspect ratio of the svg and the height
                               parameter. At least one of width and
                               height must be specified.
    :param float | None height: The height to which to scale the imported
                                lines. If None, it is calculated by the
                                aspect ratio of the svg and the width
                                parameter. At least one of width and
                                height must be specified.
    :returns: The size of the imported shapes, and the list of shapes.
              The first part is only useful if only one of width and
              height was specified.
    :rtype: (planar.Vec2, list[planar.Seq2])
    """
    if width is None and height is None:
        raise ValueError('Must specify at least one of width and height')
    svg = etree.ElementTree(file=path).getroot()
    svg_width = float(svg.get('width'))
    svg_height = float(svg.get('height'))
    shapes = []
    for element in svg:
        if element.tag in _SVG_TAGS:
            points = (s.split(',') for s in element.get('points').strip().split(' '))
            points = [planar.Vec2(float(c1), float(c2)) for c1, c2 in points]
            shapes.append(_SVG_TAGS[element.tag](points))
        elif element.tag not in _SVG_IGNORE_TAGS:
            warnings.warn(
                'Tag {} unsupported, can not be imported'.format(element.tag))
    aspect_ratio = svg_width / svg_height
    if width is None:
        width = height * aspect_ratio
    if height is None:
        height = width / aspect_ratio
    scale_matrix = planar.Affine.scale(planar.Vec2(width/svg_width,
                                                   height/svg_height))
    return planar.Vec2(width, height), [s*scale_matrix for s in shapes]
