from libraries.printrun import printcore, gcoder
from framework import properties
from . import gcode
from .. import base


class PrinterHandler(base.BasePlugin):
    """Prints changes to the board."""

    def __init__(self):
        super().__init__()
        self.gcode_exporter = gcode.GCodeExporter()
        self.printer = printcore.printcore()

    def on_render(self, render_diff):
        cmds = self.gcode_exporter.export(render_diff)
        cmds = gcoder.LightGCode(cmds)
        self.printer.startprint(cmds)

    def start(self):
        self.printer.loud = True
        self.printer.connect(properties.settings['printer.port'],
                             properties.settings['printer.baudrate'])

    def get_temp(self):
        return self.printer.get_temp()

    def get_pos(self):
        return self.printer.get_pos()

    def exit(self):
        self.printer.disconnect()
