# coding=utf-8
import json
import math

from framework import geom, properties


class GCodeExporter:
    """Exports changes to the render state to gcode to update the board."""

    def __init__(self):
        """Constructor.

        :param geom.Vec2 board_size:
            Size of the printable board area in millimeters.
        """
        self._print_area_size = geom.Vec2(*properties.settings['print_area_size'])
        self._scaling_factor = geom.Vec2(*properties.settings['printer.scaling_factor'])

    def export(self, render_diff):
        """Generates gcode to update the board to the new rendered state.

        :param gui.RenderDiff render_diff:
            The render state to convert to gcode.
        """
        generator = GCodeGenerator()
        # Flip y coordinate for gcode
        flip_y_transform = (geom.Affine.translation(geom.Vec2(0, self._print_area_size.y)) *
                            geom.Affine.scale((1, -1)))
        for shape in self._optimize_shapes(render_diff.removed):
            generator.erase_shape(shape * flip_y_transform)
        for shape in self._optimize_shapes(render_diff.new):
            generator.draw_shape(shape * flip_y_transform)
        return generator.finish()

    @staticmethod
    def _optimize_shapes(shapes):
        """Order shapes and reverse to do bottom-up and left-right printing"""
        shapes = (GCodeExporter._reverse_if_needed(s) for s in shapes)
        return sorted(shapes, key=GCodeExporter._shape_sorting_key, reverse=True)

    @staticmethod
    def _reverse_if_needed(shape):
        if shape[0].x > shape[-1].x:
            return shape.from_points(reversed(shape))
        return shape

    @staticmethod
    def _shape_sorting_key(shape):
        # Distance from origin
        return shape[0].length


class GCodeGenerator:
    """Generates gcode from shapes."""

    def __init__(self):
        self._settings = properties.settings['printer']
        self._top_y = properties.settings['print_area_size'][1]
        self._scaling_factor = geom.Vec2(*self._settings['scaling_factor'])
        self._before_first_shape = True
        self._current_pos = geom.Vec2(0, 0)
        self._last_pos = geom.Vec2(0, 0)
        self._gcode = [
            'G21',                                            # Use mm as unit
            'G90',                                            # Absolute positioning
            'M83',                                            # Set extruder to relative mode
            'M84 S0',                                         # Avoid printer falling during delays
        ]

    def finish(self):
        """Finishes the generation and returns the list of gcode commands."""
        self._retract_move_to(geom.Vec2(self._current_pos.x, self._top_y),
                              self._settings['final_retract'])
        return self._gcode

    def draw_shape(self, shape):
        """Generates gcode to draw a shape"""
        first, rest = _split_head_tail(shape)
        if self._before_first_shape:
            self._extrude_move_to(first, self._settings['starting_move_extrude'])
        else:
            self._move_to(first)
        self._start_drawing()
        for p in rest:
            self._draw_to(p)
        if isinstance(shape, geom.Polygon):
            self._draw_to(first)
        self._stop_drawing()
        self._before_first_shape = False

    def erase_shape(self, shape):
        """Generates gcode to erase a shape"""
        first, rest = _split_head_tail(shape)
        self._move_to(first)
        self._start_erasing()
        for p in rest:
            self._erase_line_to(p)
        if isinstance(shape, geom.Polygon):
            self._erase_line_to(first)
        self._stop_erasing()

    def _erase_line_to(self, pos):
        delta = pos - self._current_pos
        offset = delta.normalized() * self._settings['erase_spacing']
        offset *= geom.Affine.rotation(90)
        step = delta / 5
        self._erase_to(self._current_pos + offset)
        for i in range(5):
            direction = -1 if i % 2 == 0 else 1
            self._erase_to(self._current_pos + step + 2 * direction * offset)
        self._erase_to(self._current_pos + offset)

    def _start_erasing(self):
        self._gcode += ('M400', 'M42 S255 P14')

    def _stop_erasing(self):
        self._gcode += ('M400', 'M42 S0 P14')

    def _start_drawing(self):
        if self._before_first_shape:
            self._extrude_in_place(self._settings['starting_overextrude'])
            self._delay(self._settings['starting_overextrude_delay'])
        else:
            self._extrude_in_place(self._settings['move_extrude'])
            self._delay(self._settings['move_extrude_delay'])

    def _stop_drawing(self):
        self._retract_in_place(self._settings['move_retract'])
        self._delay(self._settings['move_retract_delay'])
        direction = (self._current_pos - self._last_pos).normalized()
        shape_end_height = self._current_pos.y
        self._move_to(self._current_pos + direction*self._settings['move_away_distance'])
        self._move_to(geom.Vec2(
            self._current_pos.x,
            shape_end_height + self._settings['move_above_distance']
        ))

    def _move_to(self, pos):
        x, y = self._to_printer(pos)
        self._gcode.append('G0 X{} Y{} F{}'.format(
            x, y, self._settings['move_speed']))
        self._last_pos = self._current_pos
        self._current_pos = pos

    def _draw_to(self, pos):
        delta = pos - self._current_pos
        extrusion = self._calc_extrusion(delta)
        x, y = self._to_printer(pos)
        self._gcode.append(
            'G1 X{} Y{} E{} F{}'.format(x, y, extrusion, self._settings['print_speed']))
        self._last_pos = self._current_pos
        self._current_pos = pos

    def _erase_to(self, pos):
        x, y = self._to_printer(pos + self._settings['eraser_offset'])
        self._gcode.append('G1 X{} Y{} F{}'.format(x,
                                                   y,
                                                   self._settings['erase_speed']))
        self._last_pos = self._current_pos
        self._current_pos = pos

    def _retract_move_to(self, pos, retract_amount):
        self._extrude_move_to(pos, -retract_amount)

    def _extrude_move_to(self, pos, extrude_amount):
        x, y = self._to_printer(pos)
        self._gcode.append('G0 X{} Y{} E{} F{}'.format(
            x,
            y,
            extrude_amount,
            self._settings['move_speed']
        ))
        self._current_pos = pos

    def _retract_in_place(self, retract_amount):
        self._extrude_in_place(-retract_amount)

    def _extrude_in_place(self, extrude_amount):
        self._gcode.append('G0 E{}'.format(extrude_amount))

    def _calc_extrusion(self, move_delta):
        used = move_delta.length * self._settings['thread_width'] * self._settings['thread_height']
        return used / (math.pi * self._settings['filament_radius']**2)


    def _delay(self, milliseconds):
        self._gcode.append('G4 P{}'.format(milliseconds))

    def _to_printer(self, pos):
        return pos * (1 / self._scaling_factor)


def _split_head_tail(iterable):
    """Splits an arbitrary iterable into the first element and the tail

    :param collections.Iterable iterable: Iterable to split
    """
    it = iter(iterable)
    first = next(it)
    return first, it
