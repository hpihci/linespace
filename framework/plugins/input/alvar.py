import json
import select
import socket
import struct
import threading
import time
import numpy as np
from framework import geom, properties, system
from framework.plugins import base


class Receiver:
    """Receiver for data send by the alvar tracking program."""

    def __init__(self, callback):
        """Constructor.

        :param callback:
            Callback called every time a position data is received.
            Arguments will be the marker and the position. Will be
            called from a background thread, and should not block it!
        """
        self._callback = callback
        self._exit_event = threading.Event()
        self._thread = threading.Thread(target=self._run)

    def start(self):
        self._thread.start()

    def exit(self):
        self._exit_event.set()
        self._thread.join(0.5)

    def _run(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(('localhost', 3100))
        sock.setblocking(0)
        unpacker = struct.Struct('=bdbddd')
        try:
            while not self._exit_event.is_set():
                if select.select([sock], [], [], 0.1)[0]:
                    data = sock.recv(unpacker.size)
                    _, __, marker_id, x, y, ___ =  unpacker.unpack(data)
                    self._callback(marker_id, geom.Vec2(x, y))
        finally:
            sock.close()


class MarkerData:
    def __init__(self, position):
        self.position = position
        self.timestamp = time.time()


class Plugin(base.BasePlugin):
    def __init__(self):
        super().__init__()
        with open('calibration.json') as f:
            calib = json.load(f)
        self._homography = np.array(calib['homography']).reshape((3, 3))
        self._offset = geom.Vec2(*calib['offset'])
        self._print_area_height = properties.settings['print_area_size'][1]
        self._receiver = Receiver(self._receive)
        self._data = {}

    def start(self):
        self._receiver.start()

    def exit(self):
        self._receiver.exit()

    def _alvar_pos_to_internal(self, pos):
        print_coord = _matrix_mult(self._homography, pos) + self._offset
        # Invert y
        return geom.Vec2(print_coord.x, self._print_area_height - print_coord.y)

    def _receive(self, marker_id, pos):
        if properties.settings['alvar.finger_id_min'] <= marker_id <= properties.settings['alvar.finger_id_max']:
            self._data = {i: d for i, d in self._data.items() if d.timestamp - time.time() < 1}
            self._data[marker_id] = MarkerData(self._alvar_pos_to_internal(pos))
            print([d.position for d in self._data.values()])
            system.report_hover([d.position for d in self._data.values()])


def _matrix_mult(matrix, pos):
    return geom.Vec2(*matrix.dot(tuple(pos) + (1,))[:-1])
