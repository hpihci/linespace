import svgwrite
# Patched etree version from svgwrite
from svgwrite.etree import etree

from framework import geom


class SvgExporter:
    def __init__(self, print_area_size):
        """Constructor.

        :param geom.Vec2 print_area_size: Size of the entire board.
        """
        self._print_area_size = print_area_size

    def export(self, render_state):
        """Exports the render state to an svg.

        :param render_state: Render state to export. Can be none.
        :returns: Svg as a string.
        """
        svg = self._create_base_svg()
        if render_state is not None:
            groups = (
                ('unchanged', render_state.unchanged),
                ('new', render_state.new),
                ('removed', render_state.removed)
            )
            for name, shapes in groups:
                group = svg.add(svg.g(id=name))
                for shape in shapes:
                    if isinstance(shape, geom.Polygon):
                        group.add(svg.polygon(tuple(p) for p in shape))
                    elif isinstance(shape, geom.Polyline):
                        group.add(svg.polyline(tuple(p) for p in shape))
                    else:
                        raise Exception('Unknown shape {}'.format(shape))
        return svg.tostring()

    def _create_base_svg(self):
        # Disable validation which is a huge cause of slowdowns
        svg = svgwrite.Drawing(debug=False)
        svg.add(svg.rect((0, 0), tuple(self._print_area_size), id='outline'))
        # Set viewbox, include a little extra to view border rectangle
        width, height = self._print_area_size
        svg.viewbox(-1, -1, width + 1, height + 1)
        return svg
