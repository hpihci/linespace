import json
import pathlib
import threading

import tornado.ioloop
import tornado.web
import tornado.websocket

from framework import geom, properties, system
from . import svg
from .. import base


class DebugServer(base.BasePlugin):
    def __init__(self):
        super().__init__()
        print_area_size = geom.Vec2(*properties.settings['print_area_size'])
        self._exporter = svg.SvgExporter(print_area_size)
        self._current_svg = self._exporter.export(None)
        self._svg_lock = threading.Lock()
        self._listener_lock = threading.Lock()
        self._server_thread = threading.Thread(target=self._run_server)
        self._event_listeners = []

    def start(self):
        self._server_thread.start()

    def exit(self):
        tornado.ioloop.IOLoop.current().stop()
        self._server_thread.join(timeout=0.5)

    def on_render(self, render_diff):
        svg_string = self._exporter.export(render_diff)
        with self._svg_lock:
            self._current_svg = svg_string
        self._publish_event('render', svg=svg_string)

    def on_hover(self, positions):
        positions = [tuple(p) for p in positions]
        self._publish_event('hover', positions=positions)

    def on_speak(self, text, voice_name, volume, rate, pitch, cancel_previous):
        self._publish_event('speak',
                            text=text,
                            voice_name=voice_name,
                            volume=volume,
                            rate=rate,
                            pitch=pitch,
                            cancel_previous=cancel_previous)

    def register_listener(self, listener):
        with self._listener_lock:
            self._event_listeners.append(listener)

    def deregister_listener(self, listener):
        with self._listener_lock:
            self._event_listeners.remove(listener)

    def get_state(self):
        with self._svg_lock:
            return self._current_svg

    def _run_server(self):
        static_dir = pathlib.Path(__file__).resolve().parent / 'static'
        index_file = static_dir / 'index.html'
        routes = [
            (r'/()', tornado.web.StaticFileHandler, {'path': str(index_file)}),
            (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': str(static_dir)}),
            (r'/fonts/(.*)', tornado.web.StaticFileHandler, {'path': str(static_dir / 'fonts')}),
            (r'/websocket', SocketHandler, {'plugin': self})
        ]
        application = tornado.web.Application(routes)
        application.listen(8080)
        tornado.ioloop.IOLoop.current().start()

    def _publish_event(self, event_type, **kwargs):
        with self._listener_lock:
            for listener in self._event_listeners:
                listener(event_type, **kwargs)


class SocketHandler(tornado.websocket.WebSocketHandler):
    def initialize(self, plugin):
        self._plugin = plugin

    def open(self):
        self._plugin.register_listener(self._on_event)

    def on_close(self):
        self._plugin.deregister_listener(self._on_event)

    def on_message(self, message):
        data = json.loads(message)
        if data['type'] == 'init':
            svg_string = self._plugin.get_state()
            self._send({
                'type': 'init',
                'svg': svg_string,
            })
        elif data['type'] == 'hover':
            positions = [geom.Vec2(*p) for p in data['positions']]
            system.report_hover(positions)
        elif data['type'] == 'speech-recognized':
            system.report_recognized_speech(data['text'])

    def _send(self, json_data):
        self.write_message(json.dumps(json_data))

    def _on_event(self, event_type, **kwargs):
        kwargs['type'] = event_type
        self._send(kwargs)
