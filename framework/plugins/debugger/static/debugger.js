(function() {
    // We keep references to the hover markers, for performance reasons,
    // because we potentially get hover data 30 times a second.
    // This way we can avoid creating more elements then necessary
    // and unnecessary DOM queries.
    var hoverMarkers = [];
    var fixedHoverPositions = [];
    var mouseHoverPosition = null;
    var coverUpEnabled = false;
    var sock = new WebSocket('ws://' + window.location.host + '/websocket');
    var speechOutEnabled = ('SpeechSynthesisUtterance' in window);
    var speechInEnabled = ('SpeechRecognition' in window || 'webkitSpeechRecognition' in window);
    var voices = {}
    var currentlyRecognizing = false;
    var recognizer = null;
    var FOOTPEDAL_KEYCODE = 66;
    var COVER_UP_RADIUS = 5;

    function distance(p1, p2) {
        var deltaX = p1[0] - p2[0];
        var deltaY = p1[1] - p2[1];
        return Math.sqrt(deltaX*deltaX + deltaY*deltaY);
    }

    function showWarning(text) {
        $('#warnings').append('<div class="warning">Warning: ' + text + '</div>');
    }

    function resetTouches() {
        fixedHoverPositions = [];
        var positions = mouseHoverPosition ? [mouseHoverPosition] : []
        sock.send(JSON.stringify({type: 'hover', positions: positions}));
    }

    function updateCoverUp() {
        var rects = $('.cover-up');
        var hoverPos = mouseHoverPosition;
        if (hoverPos === null) {
            hoverPos = [0, 0];
        }

        // Get width and height in svg units from outline
        var outline = $('#outline');
        var svg_width = parseInt(outline.attr('width'));
        var svg_height = parseInt(outline.attr('height'));

        var left = Math.max(0, hoverPos[0] - COVER_UP_RADIUS);
        var right = Math.max(0, hoverPos[0] + COVER_UP_RADIUS);
        var top = hoverPos[1] - COVER_UP_RADIUS;
        var bottom = hoverPos[1] + COVER_UP_RADIUS;

        $(rects[0]).attr({
            x: 0,
            y: 0,
            width: "100%",
            height: top
        });
        $(rects[1]).attr({
            x: 0,
            y: bottom,
            width: "100%",
            height: Math.max(0, svg_height - bottom + 1)
        });
        $(rects[2]).attr({
            x: 0,
            y: 0,
            width: left,
            height: "100%"
        });
        $(rects[3]).attr({
            x: right,
            y: 0,
            width: Math.max(0, svg_width - right + 1),
            height: "100%",
        });
    }

    function createSVGElement(type) {
        // jQuery doesn't work perfectly with SVG, so we need to work around this a little
        return $(document.createElementNS('http://www.w3.org/2000/svg', type));
    }

    function displayNewSvg(svgData) {
        // Remove markers from old svg
        $(hoverMarkers).detach();

        var svg = $(svgData);
        $('svg').replaceWith(svg);
        svg.toggleClass('cover-up-active', coverUpEnabled);

        // Add cover up rectangles
        var coverUpGroup = createSVGElement('g');
        for (var i = 0; i < 4; i++) {
            createSVGElement('rect').addClass('cover-up').appendTo(coverUpGroup);
        }

        svg.append(coverUpGroup);
        updateCoverUp();

        // Group for all hover markers
        var markerGroup = createSVGElement('g').attr('id', 'hover-markers');
        svg.append(markerGroup);
        markerGroup.append(hoverMarkers);

        // Add invisible, clickable rectangle over the svg
        var rect = createSVGElement('rect').attr({
            id: 'click-overlay',
            width: '100%',
            height: '100%',
            x: 0,
            y: 0
        }).appendTo(svg);

        var clientToSvg = function (x, y) {
            var p = svg[0].createSVGPoint();
            p.x = x;
            p.y = y;
            p = p.matrixTransform(rect[0].getScreenCTM().inverse());
            return p;
        };

        rect.on('click', function (event) {
            var p = clientToSvg(event.clientX, event.clientY);
            fixedHoverPositions.push([p.x, p.y]);
        });

        rect.on('mousemove', function (event) {
            var p = clientToSvg(event.clientX, event.clientY);
            mouseHoverPosition = [p.x, p.y]
            updateCoverUp();
            sock.send(JSON.stringify({type: 'hover',
                                      positions: [mouseHoverPosition].concat(fixedHoverPositions)}));
        });

        rect.on('mouseleave', function () {
            mouseHoverPosition = null;
            updateCoverUp();
            sock.send(JSON.stringify({type: 'hover', positions: fixedHoverPositions}));
        });
    }

    function speakText(text, voiceName, volume, rate, pitch, cancel_previous) {
        var lastTts = $('#last-tts-line');
        if(cancel_previous)
            lastTts.text(text);
        else
            lastTts.append(document.createTextNode(text));

        if (speechOutEnabled) {
            if(cancel_previous)
                speechSynthesis.cancel();
            var utterance = new SpeechSynthesisUtterance();
            utterance.text = text;
            utterance.lang = 'en-US';
            utterance.voiceURI = voices[voiceName];
            utterance.volume = volume;
            utterance.rate = rate;
            utterance.pitch = pitch;
            speechSynthesis.speak(utterance);
        }
    }

    function updateHoverMarkers(positions) {
        // Hack to remove the hover caused by the mouse while in cover-up mode
        if (coverUpEnabled && mouseHoverPosition !== null) {
            // Sort by distance from mouse and remove closest
            positions = positions.sort(function (p1, p2) {
                return distance(mouseHoverPosition, p1) - distance(mouseHoverPosition, p2);
            }).slice(1);
        }

        // Reuse as many markers as possible, because this could be called
        // up to 30 times a second with a camera attached.
        if (hoverMarkers.length > positions.length) {
            var numToRemove = hoverMarkers.length - positions.length;
            $(hoverMarkers.splice(0, numToRemove)).remove();
        }
        else if (hoverMarkers.length < positions.length) {
            var numToAdd = positions.length - hoverMarkers.length;
            var group = $('#hover-markers')
            for (var i = 0; i < numToAdd; i++) {
                var marker = createSVGElement('circle')
                    .addClass('hover-marker')
                    .attr({r: 3})
                    .appendTo(group);
                hoverMarkers.push(marker[0]);
            }
        }

        $(hoverMarkers).attr('cx', function (i) {
            return positions[i][0];
        }).attr('cy', function (i) {
            return positions[i][1];
        });
    }

    function handleRecognizedText(text) {
        sock.send(JSON.stringify({
            type: 'speech-recognized',
            text: text
        }));
        resetTouches();
    }

    function onSpeechRecognized(e) {
        if (e.results.length > 0) {
            var text = e.results[0][0].transcript;
            $('#speech-recognition-status-recognized').text(text);
            handleRecognizedText(text);
        }
    }

    function onSpeechInFormSubmit(e) {
        var textField = $(this).find('input[type=text]');
        handleRecognizedText(textField.val());
        textField.val('');
        e.preventDefault();
    }

    function onResetButtonClick() {
        resetTouches();
    }

    function onCoverUpButtonClick() {
        coverUpEnabled = !coverUpEnabled;
        $('svg').toggleClass('cover-up-active', coverUpEnabled);
    }

    function onSocketOpen() {
        sock.send(JSON.stringify({type: 'init'}));
    }

    function onSocketMessage(msg) {
        var jsonMsg = JSON.parse(msg.data);
        if (jsonMsg.type === 'init') {
            displayNewSvg(jsonMsg.svg);
        }
        else if (jsonMsg.type == 'render') {
            displayNewSvg(jsonMsg.svg);
        }
        else if (jsonMsg.type == 'speak') {
            speakText(jsonMsg.text,
                      jsonMsg.voice_name,
                      jsonMsg.volume,
                      jsonMsg.rate,
                      jsonMsg.pitch,
                      jsonMsg.cancel_previous);
        }
        else if (jsonMsg.type == 'hover') {
            updateHoverMarkers(jsonMsg.positions);
        }
    }

    function onGlobalKeydown(e) {
        var tag = e.target.tagName.toLowerCase();
        if (tag === 'input' || tag === 'textarea') {
            return;
        }

        if (e.which === FOOTPEDAL_KEYCODE && !currentlyRecognizing) {
            recognizer.start();
            currentlyRecognizing = true;
            $('#speech-recognition-status').addClass('active');
            e.preventDefault();
        }
    }

    function onGlobalKeyup(e) {
        if (e.which === FOOTPEDAL_KEYCODE && currentlyRecognizing) {
            recognizer.stop();
            currentlyRecognizing = false;
            $('#speech-recognition-status').removeClass('active');
            e.preventDefault();
        }
    }

    $(document).ready(function () {
        if (speechOutEnabled) {
            speechSynthesis.getVoices().forEach(function(voice) {
                voices[voice.name] = voice.voiceURI;
            });
        }
        else {
            showWarning('Current browser does not support speech output!');
        }

        if (speechInEnabled) {
            if (!('SpeechRecognition' in window)) {
                window.SpeechRecognition = webkitSpeechRecognition;
            }

            recognizer = new SpeechRecognition();
            recognizer.lang = 'en-US';
            recognizer.continuous = true;
            recognizer.interimResults = false;
            recognizer.onresult = onSpeechRecognized;

            $(document).on('keydown', onGlobalKeydown);

            $(document).on('keyup', onGlobalKeyup);
        }
        else {
            showWarning('Current browser does not support speech recognition!');
            $('#speech-recognition-status').addClass('disabled');
        }


        $('#reset-button').on('click', onResetButtonClick);
        $('#cover-up-button').on('click', onCoverUpButtonClick);

        $('#speech-in-form').on('submit', onSpeechInFormSubmit);

        sock.onopen = onSocketOpen;
        sock.onmessage = onSocketMessage;
    });
})();
