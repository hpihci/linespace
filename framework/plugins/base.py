from .. import system


class BasePlugin:
    def start(self):
        """Called when the system starts."""
        pass

    def exit(self):
        """Called when the system exits."""
        pass

    def on_speak(self, text, voice_name, volume, rate, pitch, cancel_previous):
        """Called when text-to-speech output is requested.

        :param str text:
            Text to be spoken.
        :param str|None voice_name:
            Name of the voice to use when speaking the text.
        :param float volume:
            Volume of the speech output. Range 0.0-1.0.
        :param float rate:
            Speaking rate of the speech output. Range 0.1-10.0.
        :param pitch pitch:
            Speaking pitch for the speech output. Range 0.0-2.0.
        :param bool cancel_previous:
            Should a previous speech output be canceled.
        """
        pass

    def on_render(self, render_diff):
        """Called when the system has been rendered.

        :param RenderDiff render_diff: Result of rendering (see system.py)
        """
        pass

    def on_hover(self, positions):
        """Called when the hovered position changed.

        :param list[geom.Vec2] positions: List of hovered positions.
        """
        pass
