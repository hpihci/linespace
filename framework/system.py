# pylint: disable=invalid-name
# Would flag all global state variables as misnamed constants
# pylint: disable=global-statement
# See below for reasons for using global variables
"""Contains the implementation for many system aspects of linespace.

Both the interface for plugins and for apps are contained in this
module as functions. Method intended for plugins are marked as such in
their documentation.
"""
import collections
import copy
import enum
import itertools
import queue
import re
import threading

from . import geom, properties

# Size of the blocks on the board which serve as the basis of assigning
# space to views
_SPACE_BLOCK_SIZE = geom.Vec2(30, 30)

# This module contains some global state. This state could be
# encapsulated in some kind of singleton system or "OS" class,
# however this would cause a lot of boilerplate code and would
# probably make the code harder to understand.

# List of registered apps, indexed by name
_registered_apps = {}

# Event queue to send messages to the main loop.
# While most methods can use locks to be thread-safe, we only want to
# call app/view/widget/plugin methods on the main thread. This is why
# we need to send some events to the main thread to be executed
_event_queue = queue.Queue()

# List of registered command callbacks
_command_callbacks = []

# Lock to make above list thread-safe
_command_callbacks_lock = threading.Lock()

# List containg the last known hovered positions
_hovered_positions = []

# List of views which were hovered based on the last set of hover
# information received
_hovered_views = set()

# Dictionary mapping from blocks of space on the board to the view
# the block is assigned to
_space_mapping = {}

# List of all active apps
_active_apps = []

# Mapping from active apps to a list of all their views
_app_view_mapping = collections.defaultdict(list)

# List of all active views
_active_views = []

# Indicates whether the user is currently selecting position(s)
# When in selection mode, usual interation with apps is blocked, until
# the user either selects the requested points or cancels the operation
_in_selection_mode = False

# Callbacks for selection mode
_selection_mode_cancel_callback = None
_selection_mode_success_callback = None

# Command for selecting position(s) in selection mode
_selection_mode_select_command = None

# Number of positions to select in selection mode
_selection_mode_num_positions = None

# Lock required to set any of the selection mode related values
_selection_mode_lock = threading.Lock()


def register_app(ctor, name):
    """Class decorator to register an app.

    :param ctor:
        Class or constructor function to create a new instance of the app.
    :param name:
        Name to listen for when user requests starting the app.
    """
    _registered_apps[name.lower()] = ctor


def speak(text, voice_name=None, volume=0.5, rate=1, pitch=1, cancel_previous=True):
    """Trigger text-to-speech with the given text.

    :param str text:
        Text to be spoken.
    :param str|None voice_name:
        Name of the voice to use when speaking the text.
    :param float volume:
        Volume of the speech output. Range 0.0-1.0.
    :param float rate:
        Speaking rate of the speech output. Range 0.1-10.0.
    :param pitch pitch:
        Speaking pitch for the speech output. Range 0.0-2.0.
    :param bool cancel_previous:
        Should a previous speech output be canceled.
    """
    _event_queue.put((_EventType.speak, text, voice_name, volume, rate, pitch, cancel_previous))


def stop_speech():
    """Stops all speech output immediatly"""
    # Avoids having to implement yet another message for plugins
    speak('')


def listen_for_command(command, when_over, callback):
    """Call callback when user gives a specific command.

    :param str command:
        Command to listen for. Case is ignored.
    :param when_over:
        Filters the list of hovered positions send to the callback.
        This can be one of the following:

            - An app
            - A view
            - A widget
            - None

        In the first three cases, only hovers over the corresponding
        object will be included in the list given to the callback. If
        there are no hovers over the object, the callback will not be
        called.
        In the last case, all positions will be included.
    :param callback:
        Callback to call when a command is recognized. Will be called
        with the following parameters:

            - A list of currently hovered positions. See when_over for
              filtering this list.
            - A list of hovered positions over unclaimed space on the board.

        Should return True if the command was handled, or False if not
        (and it therefore should be passed to other callbacks).
    :returns: lambda method to stop listening
    """
    command = command.casefold()
    def filtering_callback(text, positions, positions_over_unclaimed):
        """Filtering listener."""
        if text.casefold() == command:
            return callback(positions, positions_over_unclaimed)
        return False
    return listen(when_over, filtering_callback)


def listen_for_pattern(pattern, when_over, callback):
    """Call a callback when user gives a command matching a pattern.

    :param str pattern:
        A regular expressions (see re module) to match against commands.
        Case is ignored.
    :param when_over:
        Filters the list of hovered positions send to the callback.
        This can be one of the following:

            - An app
            - A view
            - A widget
            - None

        In the first three cases, only hovers over the corresponding
        object will be included in the list given to the callback. If
        there are no hovers over the object, the callback will not be
        called.
        In the last case, all positions will be included.
    :param callback:
        Callback to call when a command is recognized. Will be called
        with the following parameters:

            - The full recognized command
            - A list of currently hovered positions. See when_over for
              filtering this list.
            - A list of hovered positions over unclaimed space on the board.
            - A list of captures groups from the regex. Useful for
              extracting information from the command. See
              https://docs.python.org/3/library/re.html for more info.

        Should return True if the command was handled, or False if not
        (and it therefore should be passed to other callbacks).
    :returns: lambda method to stop listening
    """
    regex = re.compile(pattern, re.IGNORECASE)
    def filtering_callback(command, positions, positions_over_unclaimed):
        """Filtering listener"""
        match = regex.match(command)
        if match:
            return callback(command, positions, positions_over_unclaimed, match.groups())
        return False
    return listen(when_over, filtering_callback)


def listen_for_command_once(command, callback, when_over=None):
    """Same as listen_for_command, but triggers at most once."""
    def stop_then_callback(positions, positions_over_unclaimed):
        stop()
        return callback(positions, positions_over_unclaimed)

    stop = listen_for_command(command, when_over, callback=stop_then_callback)
    return stop


def listen_for_pattern_once(pattern, callback, when_over=None):
    """Same as listen_for_pattern, but triggers at most once."""
    def stop_then_callback(command, positions, positions_over_unclaimed, catches):
        stop()
        return callback(command, positions, positions_over_unclaimed, catches)

    stop = listen_for_pattern(pattern, when_over, callback=stop_then_callback)
    return stop


def listen_once(callback, when_over=None):
    """Same as listen, but triggers at most once."""
    def stop_then_callback(command, positions, positions_over_unclaimed):
        stop()
        return callback(command, positions, positions_over_unclaimed)

    stop = listen(when_over, callback=stop_then_callback)
    return stop


def listen(when_over, callback):
    """Call a callback whenever the user issues a command.

    :param when_over:
        Filters the list of hovered positions send to the callback.
        This can be one of the following:

            - An app
            - A view
            - A widget
            - None

        In the first three cases, only hovers over the corresponding
        object will be included in the list given to the callback. If
        there are no hovers over the object, the callback will not be
        called.
        In the last case, all positions will be included.
    :param callback:
        Callback to call when a command is recognized. Will be called
        with the following parameters:

            - The full recognized command
            - A list of currently hovered positions. See when_over for
              filtering this list.
            - A list of hovered positions over unclaimed space on the board.

        Should return True if the command was handled, or False if not
        (and it therefore should be passed to other callbacks).
    :returns: lambda method to stop listening
    """
    # Avoid circular references
    from . import app
    if when_over is None:
        pos_filter = None
    elif isinstance(when_over, app.App):
        def pos_filter(p):
            view = _space_mapping.get(_get_block(p))
            return view is not None and view.app == when_over
    elif isinstance(when_over, app.View):
        pos_filter = lambda p: (_space_mapping.get(_get_block(p)) == when_over)
    else:  # when_over is a widget
        def pos_filter(p):
            # Get area in global coordinates
            area = when_over.area
            widget = when_over
            while widget.parent is not None:
                area = widget.parent.to_external(area)
                widget = widget.parent
            return area.contains_point(p)
    def filtering_callback(command, positions, positions_over_unclaimed):
        if pos_filter is not None:
            positions = [p for p in positions if pos_filter(p)]
            if not positions:
                return False
        return callback(command, positions, positions_over_unclaimed)
    with _command_callbacks_lock:
        _command_callbacks.append(filtering_callback)
    return lambda: _command_callbacks.remove(filtering_callback)


def query_positions_from_user(prompt,
                              select_command,
                              num_positions=1,
                              success_callback=None,
                              cancel_callback=None):
    """Queries the user for position(s) while blocking other interactions.

    This is especially useful for selecting the position/size of
    new/moved views.

    :param prompt:
        Text prompt to explain which positions to select.
    :param select_command:
        Command the user should input to select the positions(s).
    :param num_positions:
        Number of positions to select. These are selected at a time,
        not one after the other.
    :param success_callback:
        Callback which will be called with a list of selected positions
        if selection was successful.
    :param cancel_callback:
        Callback to call should the user cancel the operation.
    """
    with _selection_mode_lock:
        global _in_selection_mode
        global _selection_mode_cancel_callback
        global _selection_mode_success_callback
        global _selection_mode_select_command
        global _selection_mode_num_positions
        _in_selection_mode = True
        _selection_mode_cancel_callback = cancel_callback
        _selection_mode_success_callback = success_callback
        _selection_mode_select_command = select_command
        _selection_mode_num_positions = num_positions
    speak(prompt)


def report_hover(positions):
    """Reports hovered positions to the system.

    For use by plugins.
    """
    _event_queue.put((_EventType.hover, positions))


def report_recognized_speech(text):
    """Reports recognized speech to the system.

    For use by plugins.
    """
    _event_queue.put((_EventType.speech_recognized, text))


def run(plugins):
    """Run the linespace system until its exited.

    One convenient way while developing to exit the system is just
    using Ctrl-C. This methods catches the signal and cleanly exits the
    system.

    :param plugins:
        List of plugins to use. These plugins can provide interaction
        with hardware for in-/output.
    """
    # Import app registrations
    # pylint: disable=unused-variable
    import apps
    plugins = list(plugins)
    board_rect = geom.rect(
        geom.Vec2(0, 0),
        geom.Vec2(*properties.settings['print_area_size'])
    )
    listen_for_pattern(
        'Launch (.*)',
        None,
        lambda _, __, ___, matches: _on_launch_app(matches[0]))
    for plugin in plugins:
        plugin.start()
    currently_rendered = _render(plugins, set())
    try:
        while True:
            event_type, *event_args = _event_queue.get()
            if event_type == _EventType.hover:
                _handle_hover(plugins, event_args[0])
            elif event_type == _EventType.speak:
                for p in plugins:
                    p.on_speak(*event_args)
            elif event_type == _EventType.speech_recognized:
                if _handle_recognized_speech(event_args[0]):
                    currently_rendered = _render(plugins,
                                                 currently_rendered)
    except KeyboardInterrupt:
        # Clean exit on Ctrl-C
        pass
    finally:
        for plugin in plugins:
            plugin.exit()


def _handle_hover(plugins, positions):
    """Handles a reported hover.

    :param plugins:
        List of active plugins.
    :param positions:
        List of reported hovered positions
    """
    global _hovered_positions, _hovered_views
    for plugin in plugins:
        plugin.on_hover(positions)
    if not _in_selection_mode:
        with_views = ((p, _space_mapping.get(_get_block(p))) for p in positions)
        # Convert to list so we can later reuse to find all views
        with_views = [(p, v) for p, v in with_views if v is not None]
        # Sort by view to then group by view
        sorted_data = sorted(with_views, key=lambda t: id(t[1]))
        for view, tuples in itertools.groupby(sorted_data,
                                              lambda t: t[1]):
            positions_over_view = [p for p, _ in tuples]
            if view not in _hovered_views:
                # Same as for containers:
                # If multiple touches enter in the same update,
                # we just use one of them
                view.container.hover_enter(positions_over_view[0])
            view.container.hover(*positions_over_view)
        views = {v for _, v in with_views}
        for view in _hovered_views - views:
            view.container.hover_exit()
        _hovered_views = views
    _hovered_positions = positions


def _handle_recognized_speech(text):
    """Handles reported recognized speech.

    :param str text:
        Recognized text
    :returns:
        Whether any callback accepted the speech.
    """
    # No lock necessary, only accessed from the main thread
    positions = copy.copy(_hovered_positions)
    if _in_selection_mode:
        return _handle_speech_in_selection_mode(text, positions)
    # We create a copy of the callback list, so that we don't
    # need to hold the lock while executing callbacks
    # Otherwise the system deadlocks when attaching handlers
    # in speech callbacks
    with _command_callbacks_lock:
        callbacks = copy.copy(_command_callbacks)
    over_unclaimed = [p for p in positions if _get_block(p) not in _space_mapping]
    over_claimed = [p for p in positions if _get_block(p) in _space_mapping]
    for callback in callbacks:
        if callback(text, over_claimed, over_unclaimed):
            return True
    speak('Unknown command: {}'.format(text))
    return False


def _handle_speech_in_selection_mode(text, hovered_positions):
    """Handles speech input while in selection mode"""
    text = text.casefold()
    if text == 'cancel':
        cb = _selection_mode_cancel_callback
        _end_selection_mode()
        if cb is not None:
            cb()
        return True
    elif text == _selection_mode_select_command:
        if len(hovered_positions) != _selection_mode_num_positions:
            if _selection_mode_num_positions == 1:
                speak('Please select 1 point instead of {}'.format(
                    len(hovered_positions)))
            else:
                speak('Please select {} points instead of {}'.format(
                    _selection_mode_num_positions,
                    len(hovered_positions)))
            return False
        cb = _selection_mode_success_callback
        _end_selection_mode()
        if cb is not None:
            cb(hovered_positions)
        return True
    speak('Unknown command: {}'.format(text))
    return False


def _end_selection_mode():
    """Cleanly ends selection mode and resets all associated variables"""
    with _selection_mode_lock:
        global _in_selection_mode
        global _selection_mode_cancel_callback
        global _selection_mode_success_callback
        global _selection_mode_select_command
        global _selection_mode_num_positions
        _in_selection_mode = False
        _selection_mode_cancel_callback = None
        _selection_mode_success_callback = None
        _selection_mode_select_command = None
        _selection_mode_num_positions = None


def _render(plugins, currently_rendered):
    """Rerenders the application and sends the changes to all plugins.

    :param container:
        Container containing all widgets in the entire system.
    :param plugins:
        List of active plugins.
    :param currently_rendered:
        Set of currently rendered shapes.
    :returns:
        Set of now rendered shapes
    """
    view_renderings = (v.container.render() for v in _active_views)
    rendered = itertools.chain(*view_renderings)
    new_state = _to_shape_set(_normalize_shapes(rendered))
    # Have to convert to list here, otherwise there would be double
    # evaluation of generators
    added = new_state.difference(currently_rendered)
    removed = currently_rendered.difference(new_state)
    unchanged = currently_rendered.difference(removed)
    diff = RenderDiff(
        unchanged=_unpack_shapes(unchanged),
        new=_unpack_shapes(added),
        removed=_unpack_shapes(removed)
    )
    if diff.new or diff.removed:
        for plugin in plugins:
            plugin.on_render(diff)
    return new_state


def _on_launch_app(app_name):
    """Callback when a launch app command is recognized."""
    app_name = app_name.lower()
    app_cls = _registered_apps.get(app_name)
    if app_cls is None:
        speak('App name {} not recognized'.format(app_name))
    else:
        _active_apps.append(app_cls())
    return True


def _request_space(view, shapes):
    """Internal code for View.request_space"""
    blocks = set()
    for shape in shapes:
        bbox = shape.bounding_box
        if not _is_bbox_on_board(bbox):
            raise ValueError('Shape does not fit on the board!')
        min_block = bbox.min_point // _SPACE_BLOCK_SIZE
        max_block = bbox.max_point // _SPACE_BLOCK_SIZE
        for x in range(int(min_block.x), int(max_block.x) + 1):
            for y in range(int(min_block.y), int(max_block.y) + 1):
                blocks.add(geom.Vec2(x, y))
    if not all(_space_mapping.get(b) in {None, view} for b in blocks):
        return False
    for block in blocks:
        _space_mapping[block] = view
    return True


def _register_view(view):
    """Called by view constructor to register itself"""
    _active_views.append(view)
    _app_view_mapping[view.app].append(view)


def _close_view(view):
    """Closes a view and frees all resources associated with it"""
    # Not the most efficient, but should be still good enough
    # Need to convert to list, otherwise we would change a dictionary
    # while iterating over it
    blocks = [b for b, v in _space_mapping.items() if v == view]
    for block in blocks:
        del _space_mapping[block]
    _app_view_mapping[view.app].remove(view)


def _close_app(app):
    """Closes the app and all its views"""
    # Copy because we are deleting elements from the list in the loop
    for view in copy.copy(_app_view_mapping[app]):
        _close_view(view)


def _get_block(pos):
    """Gets the block in which a position lies."""
    return pos // _SPACE_BLOCK_SIZE


def _is_bbox_on_board(bbox):
    """Checks whether a given bounding box is fully on the board area"""
    board_size = geom.Vec2(*properties.settings['print_area_size'])
    return (bbox.min_point.x >= 0 and
            bbox.min_point.y >= 0 and
            bbox.max_point.x < board_size.x and
            bbox.max_point.y < board_size.y)


class _EventType(enum.Enum):
    """Used for the internal, asynchronous event queue."""
    hover = 0
    speak = 1
    speech_recognized = 2


class _HashedShape:
    """Helper class for comparing shapes with the help of hashing.

    Assumes the polygons/-lines don't change during the usage of
    the associated object of this class.
    """
    def __init__(self, shape):
        self.shape = shape
        self._hash = None

    def __hash__(self):
        if self._hash is None:
            if isinstance(self.shape, (geom.Polyline, geom.Polygon)):
                self._hash = hash(tuple(self.shape))
            else:
                self._hash = hash(self.shape)
        return self._hash

    def __eq__(self, other):
        return (isinstance(other, _HashedShape) and
                self.shape == other.shape)

    def __ne__(self, other):
        return not self == other


class RenderDiff:
    """Represents the rendering state of the system compared to the previous.

    Contains the added, removed and unchanged shapes as lists. Possible
    types of shapes are polygons, polylines and circles.
    """

    def __init__(self, unchanged, new, removed):
        """Constructor. See class doc for property documentation.

        :type unchanged: collections.Iterable
        :type new: collections.Iterable
        :type removed: collections.Iterable
        """
        self.unchanged = list(unchanged)
        self.new = list(new)
        self.removed = list(removed)


def _unpack_shapes(hashed_shapes):
    """Unpacks an iterable of hashed shapes to the original shapes."""
    return (s.shape for s in hashed_shapes)


def _normalize_shapes(shapes):
    """Normalizes an iterable of shapes to only polygons, polylines and circles."""
    return ((s.to_polygon() if isinstance(s, geom.BoundingBox) else s) for s in shapes)


def _to_shape_set(shapes):
    """Converts an iterable of shapes to a set of hashed shapes."""
    return set(_HashedShape(s) for s in shapes)
