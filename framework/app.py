"""Creates functionality for implementing apps in the linespace system."""
from . import geom, widget, system


class App:
    """Base class for all apps in the system."""
    def close(self):
        """Closes the app and all its views"""
        system._close_app(self)


class View:
    """Base class for all views.

    Views are basically independent regions on the board reserved for
    an app. They can request to be expanded, and also give up reserved
    space.

    All views have an attribute `container`, which is a container
    widget to which all widgets contained in the view must be added.
    """

    def __init__(self, app):
        """Constructor.

        :param app:
            App the view belongs to.
        """
        self.app = app
        self.container = widget.ContainerWidget(geom.AllArea())
        system._register_view(self)

    def request_space(self, *shapes):
        """Tries to claim space on the board for this view.

        This operation fails if some part of the requested space is
        already claimed by another view.

        :param shapes:
            One or more shapes to reserve the space for. This can
            simply be a rectangle, or several shapes that should
            be printed next.
        :returns:
            True if the space was successfully allocated, False if not.
        """
        return system._request_space(self, shapes)

    def close(self):
        """Closes the view and frees up all claimed space"""
        system._close_view(self)
