"""Provides widget base classes and helpful widget containers."""
import collections
import enum
import itertools

from . import geom


class Widget:
    """Base class for widgets.

    Contains default implementations for all methods/properties.
    """

    def __init__(self, area):
        """Constructor.

        :param geom.BoundingBox area:
            Area that the widget occupies.
        """
        self.area = area
        # Set by a container when added
        self.parent = None

    def hover_enter(self, pos):
        """Called when the widget begins being hovered.

        :param geom.Vec2 pos: Position of hover.
        """
        pass

    def hover(self, *positions):
        """Called whenever the widget is hovered at new positions.

        :param tuple(geom.Vec2) positions: All hovered positions over the widget.
        """
        pass

    def hover_exit(self):
        """Called when the widget stops being hovered."""
        pass

    # pylint: disable=no-self-use
    def render(self):
        """Called whenever the widget needs to be rendered.

        :returns: An iterable of shapes representing the widget.
        """
        return ()


class StaticWidget(Widget):
    """Base class for widgets with static rendering representation"""

    def __init__(self, area, shapes):
        """Constructor.

        :param shapes: Iterable of shapes to represent the widget.
        """
        super().__init__(area)
        self._shapes = shapes

    def render(self):
        return self._shapes


class ContainerWidget(Widget):
    """Widget that contains other widgets."""

    def __init__(self, area):
        """Constructor.

        :param area:
            Area of this container. This must include the area of all
            children widgets!
        """
        super().__init__(area)
        self._children = []
        self._hovered_children = set()

    def add(self, child):
        """Adds a child to the container"""
        child.parent = self
        self._children.append(child)

    def remove(self, child):
        """Removes a child from the container."""
        self._children.remove(child)
        child.parent = None

    def to_external(self, shape):  # pylint: disable=no-self-use
        """Converts a shape from internal coordinates to external ones.

        Doesn't do anything on the standard container, only for
        implementation in derived ones with more complex layout logic.
        """
        return shape

    def hover(self, *positions):
        hovered = ((pos, self._child_at(pos)) for pos in positions)
        hovers_per_child = collections.defaultdict(list)
        for pos, child in ((p, c) for p, c in hovered if c is not None):
            hovers_per_child[child].append(pos)
        for child, positions in hovers_per_child.items():
            if child not in self._hovered_children:
                # If multiple hovers appear in the same update
                # we just take the first one
                child.hover_enter(positions[0])
            child.hover(*positions)
        hovered_set = set(hovers_per_child.keys())
        for removed_child in self._hovered_children.difference(hovered_set):
            removed_child.hover_exit()
        self._hovered_children = hovered_set

    def hover_exit(self):
        for child in self._hovered_children:
            child.hover_exit()
        self._hovered_children.clear()

    def render(self):
        return itertools.chain.from_iterable(c.render() for c in self._children)

    def _child_at(self, pos):
        """Finds the first child widget that contains a certain point.

        :param geom.Vec2 pos: Position to test for.
        :rtype: Widget | None
        """
        return next((c for c in self._children if c.area.contains_point(pos)),
                    None)


class CoordinateSystemContainer(ContainerWidget):
    """Container with an internal coordinate system."""

    def __init__(self, area, internal_area):
        """Constructor.

        :param geom.BoundingBox area:
            Box in the same coordinate system as the container, that
            will be mapped to dest_box in the internal coordinate
            system.
        :param geom.BoundingBox internal_area:
            Box in the internal coordinate system. It will be mapped
            to the origin_box, but it's aspect ratio will be preserved.
            If one dimension of the origin box is not fully used
            (because of aspect ratio preservation), it will be centered
            inside that dimension.
        """
        scale = min(area.width / internal_area.width,
                    area.height / internal_area.height)
        self._to_internal = (geom.Affine.translation(internal_area.center) *
                             geom.Affine.scale(1 / scale) *
                             geom.Affine.translation(-area.center))
        self._to_external = (geom.Affine.translation(area.center) *
                             geom.Affine.scale(scale) *
                             geom.Affine.translation(-internal_area.center))
        actual_area = self._to_external * internal_area
        super().__init__(actual_area)

    def hover_enter(self, pos):
        super().hover_enter(pos * self._to_internal)

    def hover(self, *positions):
        super().hover(*(p * self._to_internal for p in positions))

    def render(self):
        return (s * self._to_external for s in super().render())

    def to_external(self, shape):
        return shape * self._to_external


class LayoutDirection(enum.Enum):
    """Enum defining the layout direction in a StackContainer."""
    vertical = 0
    horizontal = 1


class OutOfSpaceError(Exception):
    """Raised if more space than available is requested from a layout container."""
    pass


class StackContainer(ContainerWidget):
    """Container to horizontally/vertically layout child widgets."""

    # pylint: disable=too-many-arguments
    def __init__(self,
                 box,
                 direction,
                 margin_left=0,
                 margin_top=0,
                 margin_right=0,
                 margin_bottom=0):
        """Constructor.

        :param BoundingBox box: Area to layout children in.
        :param LayoutDirection direction: Main direction of the layout.
        :param float margin_left: Left margin of every child widget.
        :param float margin_top: Top margin of every child widget.
        :param float margin_right: Right margin of every child widget.
        :param float margin_bottom: Bottom margin of every child widget.
        """
        super().__init__(box)
        self._direction = direction
        self._margin_left = margin_left
        self._margin_top = margin_top
        self._margin_right = margin_right
        self._margin_bottom = margin_bottom
        if direction == LayoutDirection.horizontal:
            self._remaining_space = box.width
        else:
            self._remaining_space = box.height

    def add(self, space, create_child):  # pylint: disable=arguments-differ
        """Adds and arranges a new child widget.

        :param float space:
            Amount of space to reserve for the child in the main layout direction
            (i.e. width if horizontal, height if vertical).
        :param create_child:
            Function to create the child widget. Will be given the reserved
            space as an argument of type BoundingBox. This does *not*
            include the margins specified in the constructor.
        :raises: OutOfSpaceError
        :returns: The newly created child
        """
        if self._direction == LayoutDirection.horizontal:
            total_space = space + self._margin_left + self._margin_right
        else:
            total_space = space + self._margin_top + self._margin_bottom
        if total_space > self._remaining_space:
            raise OutOfSpaceError('Would need {} space, {} available'.format(
                total_space, self._remaining_space))
        if self._direction == LayoutDirection.horizontal:
            x = self.area.width - self._remaining_space + self._margin_left
            y = self._margin_top
            width = space
            height = self.area.height - self._margin_top - self._margin_bottom
        else:
            x = self._margin_left
            y = self.area.height - self._remaining_space + self._margin_top
            width = self.area.width - self._margin_left - self._margin_right
            height = space
        self._remaining_space -= total_space
        child = create_child(geom.rect(geom.Vec2(x, y) + self.area.min_point,
                                       geom.Vec2(width, height)))
        super().add(child)
        return child


class DockingSide(enum.Enum):
    """Side at which to dock a widget in a DockContainer."""
    left = 0
    top = 1
    right = 2
    bottom = 3


class DockContainer(ContainerWidget):
    """Container for docking widgets around a centered filling widget."""

    def __init__(self, box):
        """Constructor.

        :param geom.BoundingBox box: Rectangle to layout children in.
        """
        super().__init__(box)
        self._remaining_rect = box

    def add(self, side, space, create_child):  # pylint: disable=arguments-differ
        """Adds a new docked child.

        :param DockingSide side:
            Side to dock the new child at.
        :param float space:
            Amount of space to reserve for the new child. This corresponds to
            width for left/right docking, and height for top/bottom.
        :param create_child:
            Function to create the new child element. The reserved rectangle
            will be given as an argument.
        :raises: OutOfSpaceError
        :returns:
            The newly created child.
        """
        if self._remaining_rect is None:
            raise OutOfSpaceError('Dock container is already filled')
        if side == DockingSide.left or side == DockingSide.right:
            if space > self._remaining_rect.width:
                raise OutOfSpaceError('Only {} width left, {} requested'.format(
                    self._remaining_rect.width, space))
            size = geom.Vec2(space, self._remaining_rect.height)
            remaining_size = geom.Vec2(self._remaining_rect.width - space,
                                       self._remaining_rect.height)
            if side == DockingSide.left:
                pos = self._remaining_rect.min_point
                remaining_pos = self._remaining_rect.min_point + geom.Vec2(space, 0)
            else:
                pos = geom.Vec2(self._remaining_rect.max_point.x - space,
                                self._remaining_rect.min_point.y)
                remaining_pos = self._remaining_rect.min_point
        else:
            if space > self._remaining_rect.height:
                raise OutOfSpaceError('Only {} height left, {} requested'.format(
                    self._remaining_rect.height, space))
            size = geom.Vec2(self._remaining_rect.width, space)
            remaining_size = geom.Vec2(self._remaining_rect.width,
                                       self._remaining_rect.height - space)
            if side == DockingSide.top:
                pos = self._remaining_rect.min_point
                remaining_pos = self._remaining_rect.min_point + geom.Vec2(0, space)
            else:
                pos = geom.Vec2(self._remaining_rect.min_point.x,
                                self._remaining_rect.max_point.y - space)
                remaining_pos = self._remaining_rect.min_point
        self._remaining_rect = geom.rect(remaining_pos, remaining_size)
        child_rect = geom.rect(pos, size)
        child = create_child(child_rect)
        super().add(child)
        return child

    def add_fill(self, create_child):
        """Adds a filling element into the remaining space of the container.

        :param create_child:
            Function to create the child widget. The rectangle to fill
            will be given as an argument to this function.
        :raises: OutOfSpaceError
        :returns:
            The newly created child.
        """
        if self._remaining_rect is None:
            raise OutOfSpaceError('Dock container is already filled')
        child = create_child(self._remaining_rect)
        self._remaining_rect = None
        super().add(child)
        return child
