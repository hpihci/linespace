#include <cstdlib>
#include <iostream>
#include <vector>
#include <opencv2/calib3d/calib3d.hpp>


void printError(const char* program_name) {
    std::cerr << "Usage: "
              << program_name
              << " x11 y11 x12 y12 x13 y13 x14 y14 x21 y21 x22 y22 x23 y23 x24 y24\n";
}


int main(int argc, const char* argv[]) {
    if (argc != 17) {
        printError(argv[0]);
        return 1;
    }

    std::vector<cv::Point2f> pointLists[2];
    for (int i = 0; i < 2; i++) {
        pointLists[i] = std::vector<cv::Point2f>(4);
        for (int j = 0; j < 4; j++) {
            pointLists[i][j] = {
                std::strtof(argv[8*i + 2*j + 1], nullptr),
                std::strtof(argv[8*i + 2*j + 2], nullptr)
            };
        }
    }

    auto mat = cv::findHomography(pointLists[0], pointLists[1], CV_RANSAC);
    std::cout << cv::format(mat, "csv") << '\n';
    return 0;
}
