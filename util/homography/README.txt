This program exists for calculating the homography during the camera setup.

Because the alvar tracking program needs opencv 2.4, we would have to install
opencv 3 to use opencv in python to calculate the homography, which would be
annoying and could lead to errors.

The other option would be to write the calculation ourself, but that would
also be kind of error prone.

This solution allows us to re-use the opencv installation already required
for alvar marker tracking.
