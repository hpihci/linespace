#!/usr/bin/env python3
import argparse
import re
import sys

import xml.etree.ElementTree as etree

import svgwrite
from inkex import inkex, simpletransform, cubicsuperpath, cspsubdiv, bezmisc

PROGRAM_DESCRIPTION = '''Simplifies an SVG for importing it with geom.import_svg().

The SVG must meet the following conditions:
    - Have valid "width" and "height" attributes specified. These are usually
      referred to as canvas size in editors.
    - Only use paths (use object to path in an svg editor).
    - Not use units (units will be ignored, with a warning issued).
'''
CSS_TEMPLATE = '''* {{
    fill: none;
    stroke: black;
    stroke-width: {};
}}
'''
LENGTH_REGEX = re.compile('^([0-9]*(?:\.[0-9]*)?)(.*)$')
IGNORE_ELEMENTS = {
    inkex.addNS('defs', 'svg'),
    inkex.addNS('metadata', 'svg'),
    inkex.addNS('namedview', 'sodipodi')
}


class SimplifyError(Exception):
    def __init__(self, message):
        self.message = message


def warn(message):
    print('Warning: ' + message, file=sys.stderr)


def traverse(out_svg, element, parent_matrix, parent_visibility, accuracy, off_x, off_y):
    """Traverses a input svg element recursively

    :param svgwrite.Drawing out_svg: Svg to write simplified lines to
    :param etree.Element element: Input element
    :param list[list[float]] parent_matrix: Transformmatrix of the parent element
    :param str parent_visibility: Visibility of the parent element
    """
    num_polygons = 0
    num_points = 0

    visibility = element.get('visibility', parent_visibility)
    if visibility == 'inherit':
        visibility = parent_visibility
    if visibility in {'hidden', 'collapse'}:
        return num_polygons, num_points
    display = element.get('display')
    if display == 'none':
        return num_polygons, num_points

    transform = element.get('transform')
    matrix = simpletransform.composeTransform(parent_matrix,
                                              simpletransform.parseTransform(transform))

    if is_svg_tag(element, 'g') or is_svg_tag(element, 'svg'):
        for child in element:
            child_polygons, child_points = traverse(out_svg, child, matrix, visibility, accuracy, off_x, off_y)
            num_polygons += child_polygons
            num_points += child_points
    elif is_svg_tag(element, 'path'):
        data = element.get('d')
        path = cubicsuperpath.parsePath(data)
        simpletransform.applyTransformToPath(matrix, path)
        for subpath in path:
            subdivide_cubic_path(subpath, accuracy)
            points = [[csp[1][0] + off_x, csp[1][1] + off_y] for csp in subpath]
            if len(points) > 1:
                if points[0] == points[-1]:
                    out_svg.add(out_svg.polygon(points[:-1]))
                else:
                    out_svg.add(out_svg.polyline(points))
            num_polygons += 1
            num_points += len(points)
    elif element.tag not in IGNORE_ELEMENTS:
        warn('Unsupported <{}> tag found! Please convert SVG to paths!.'.format(element.tag))
    return num_polygons, num_points


def parse_length(length):
    """Tries to parse a svg length with optional units.

    :param str length: Length in string form
    :rtype: float
    """
    try:
        return float(length)
    except ValueError:
        match = LENGTH_REGEX.match(length)
        if not match:
            raise ValueError('Couldnt parse length: ' + length)
        warn('Units are not supported! Removing {} from {}'.format(match.group(2), length))
        return float(match.group(1))


def is_svg_tag(element, tag):
    """Checks whether an element is a specific svg tag.

    :param etree.Element element: Element to check
    :param str tag: Tag to check for
    """
    return element.tag in {inkex.addNS(tag, 'svg'), tag}


# Taken from https://github.com/martymcguire/inkscape-unicorn svg_parser.py
def subdivide_cubic_path(sp, flat, i=1):
    """
    Break up a bezier curve into smaller curves, each of which
    is approximately a straight line within a given tolerance
    (the "smoothness" defined by [flat]).

    This is a modified version of cspsubdiv.cspsubdiv(). I rewrote the recursive
    call because it caused recursion-depth errors on complicated line segments.
    """
    while True:
        while True:
            if i >= len(sp):
                return
            p0 = sp[i - 1][1]
            p1 = sp[i - 1][2]
            p2 = sp[i][0]
            p3 = sp[i][1]
            b = (p0, p1, p2, p3)
            if cspsubdiv.maxdist(b) > flat:
                break
            i += 1
        one, two = bezmisc.beziersplitatt(b, 0.5)
        sp[i - 1][2] = one[1]
        sp[i][0] = two[2]
        p = [one[2], one[3], two[1]]
        sp[i:1] = [p]


def simplify(in_svg_path, out_svg_path, accuracy):
    """Simplifies a svg

    :param str in_svg_path: Path to the input svg
    :param str out_svg_path: Path to save the simplified svg to
    :param float accuracy: Accuracy of the path conversion
    """
    try:
        in_svg = etree.ElementTree(file=in_svg_path).getroot()
    except etree.ParseError as e:
        raise SimplifyError("Couldn't parse input file as an svg!") from e
    x = y = 0
    if 'width' not in in_svg.attrib or 'height' not in in_svg.attrib:
        if 'viewBox' not in in_svg.attrib:
            raise SimplifyError('Input svgs must have width and height or viewBox specified!')
        x, y, width, height = (parse_length(s) for s in in_svg.get('viewBox').split(' '))
    else:
        width = parse_length(in_svg.get('width'))
        height = parse_length(in_svg.get('height'))
    out_svg = svgwrite.Drawing(size=(width, height))
    # Some reasonable estimation for a good stroke width
    stroke_width = max(width, height) / 200
    out_svg.defs.add(out_svg.style(CSS_TEMPLATE.format(stroke_width)))
    num_polygons, num_points = traverse(out_svg, in_svg, [[1, 0, 0], [0, 1, 0]], 'visible', accuracy, -x, -y)
    out_svg.saveas(out_svg_path)
    print('Created {} polygons/-lines, with a total of {} points'.format(num_polygons, num_points))


def main():
    parser = argparse.ArgumentParser(
        description=PROGRAM_DESCRIPTION,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('input', help='The SVG file to convert')
    parser.add_argument('output', help='Where to write the converted SVG to')
    parser.add_argument('-a', '--accuracy', help='Accuracy of the path conversion', type=float, default=0.2)
    args = parser.parse_args()
    try:
        simplify(args.input, args.output, args.accuracy)
    except SimplifyError as e:
        print('Error: ' + e.message)
        sys.exit(1)


if __name__ == '__main__':
    main()
