Simplifies svgs to only contain lines (<polygon>, <polyline>) and to have no transforms.

Based upon the inkscape extension api and the inkscape svg to gcode converter (https://github.com/martymcguire/inkscape-unicorn)