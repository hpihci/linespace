#!/usr/bin/env python3
import logging

from framework import system
from framework.plugins import debugger


if __name__ == '__main__':
    # Minimal logging output and reference to debugger
    logging.basicConfig(level=logging.ERROR)
    print('Debugger now available at http://localhost:8080')
    system.run((
        debugger.DebugServer(),
    ))
