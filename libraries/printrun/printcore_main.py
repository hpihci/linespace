#!/usr/bin/env python

# This file is part of the Printrun suite.
#
# Printrun is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Printrun is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Printrun.  If not, see <http://www.gnu.org/licenses/>.

import time
import getopt
import sys

from printcore import printcore
import gcoder
from utils import setup_logging

if __name__ == '__main__':
    setup_logging(sys.stderr)
    baud = 250000
    statusreport = False
    port = 'COM3'
    try:

        opts, args = getopt.getopt(sys.argv[1:], 'hcrn:l:wo:a:emi', ["reset="])
    except getopt.GetoptError, err:
        print str(err)
        sys.exit(2)
    for o, a in opts:
        if o in ('-h', '--help'):
            # FIXME: Fix help
            print ("Opts are: --help, -b --baud = baudrate, -v --verbose, "
                   "-s --statusreport")
            sys.exit(1)
        if o in ('-v', '--verbose'):
            loud = True
        elif o in ('-s', '--statusreport'):
            statusreport = True

    # Hack to get around the 8192 character limit for command line invocations
    with open('out.gcode') as f:
        gcode = f.read().split('\n')

    #if len(args) > 1:

    #    print "Printing: on %s with baudrate %d" % (port, baud)
    #else:
    #    print "Usage: python [-h|-b|-v|-s] printcore.py /dev/tty[USB|ACM]x filename.gcode"
        #sys.exit(2)
    p = printcore(port, baud)
    p.loud = True
    time.sleep(2)
    gcode = gcoder.LightGCode(gcode)

    p.startprint(gcode)

    try:
        if statusreport:
            p.loud = True
            sys.stdout.write("Progress: 00.0%\r")
            sys.stdout.flush()
        while p.printing:
            time.sleep(1)
            if statusreport:
                progress = 100 * float(p.queueindex) / len(p.mainqueue)
                sys.stdout.write("Progress: %02.1f%%\r" % progress)
                sys.stdout.flush()
        p.disconnect()
        sys.exit(0)
    except:
        p.disconnect()
