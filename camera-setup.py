#!/usr/bin/env python3
import itertools
import json
import subprocess
import sys
import threading
import time
import numpy as np
from framework import geom, properties
from framework.plugins.input import alvar
from libraries.printrun import printcore

WIDTH, HEIGHT = properties.settings['print_area_size']
PRINTER_POINTS = [
        geom.Vec2(WIDTH, 0),
        geom.Vec2(WIDTH, HEIGHT),
        geom.Vec2(0, HEIGHT),
        geom.Vec2(0, 0)
]


class PositionReceiver:
    def __init__(self):
        self._receiver = alvar.Receiver(self._callback)
        self._lock = threading.Lock()
        self._printer_pos = None
        self._bottom_left_ref_pos = None
        self._aux_pos = None

    def __enter__(self):
        self._receiver.start()
        return self

    def __exit__(self, *_):
        self._receiver.exit()

    @property
    def printer_pos(self):
        with self._lock:
            return self._printer_pos

    @printer_pos.setter
    def printer_pos(self, value):
        with self._lock:
            self._printer_pos = value

    @property
    def bottom_left_ref_pos(self):
        with self._lock:
            return self._bottom_left_ref_pos

    @bottom_left_ref_pos.setter
    def bottom_left_ref_pos(self, value):
        with self._lock:
            self._bottom_left_ref_pos = value

    @property
    def aux_pos(self):
        with self._lock:
            return self._aux_pos

    @aux_pos.setter
    def aux_pos(self, value):
        with self._lock:
            self._aux_pos = value

    def _callback(self, marker_id, pos):
        if marker_id == properties.settings['alvar.printer_id']:
            self.printer_pos = pos
        elif marker_id == properties.settings['alvar.bottom_left_ref_id']:
            self.bottom_left_ref_pos = pos
        elif marker_id == properties.settings['alvar.aux_id']:
            self.aux_pos = pos


def collect_points():
    collected_printer_points = []

    printer = printcore.printcore(
            properties.settings['printer.port'],
            properties.settings['printer.baudrate']
    )
    time.sleep(0.5)
    bottom_left_ref_pos = None
    aux_pos = None
    with PositionReceiver() as receiver:
        for printer_pos in PRINTER_POINTS:
            printer.send_now('G0 X{} Y{} F{}'.format(
                printer_pos.x,
                printer_pos.y,
                properties.settings['printer.move_speed']
            ))
            while True:
                pos_current, pos_target = printer.get_pos()
                if pos_current == pos_target:
                    break
            time.sleep(1)
            if receiver.printer_pos is None:
                print('No data from alvar tracking received!', file=sys.stderr)
                sys.exit()
            else:
                collected_printer_points.append(receiver.printer_pos)
                if aux_pos is None:
                    bottom_left_ref_pos = receiver.bottom_left_ref_pos
                    aux_pos = receiver.aux_pos
        printer.disconnect()
        return (collected_printer_points,
                bottom_left_ref_pos,
                aux_pos)


def matrix_mult(matrix, point):
    return geom.Vec2(*matrix.dot(tuple(point) + (1,))[:-1])


def calc_and_save_calibration(printer_positions, bottom_left_ref_pos, aux_pos):
    cmd = ['util/homography/build/CalcHomography']
    for point_list in (printer_positions, PRINTER_POINTS):
        for point in point_list:
            cmd.append(str(point.x))
            cmd.append(str(point.y))
    out_bytes = subprocess.check_output(cmd)
    splits = out_bytes.decode().replace('\n', ',').split(',')
    numbers = [float(s.strip()) for s in splits if s]
    homography = np.array(numbers).reshape((3, 3))
    print('Alvar positions:')
    print('\tAux pos: ' + str(aux_pos))
    print('\tLeft bottom pos: ' + str(bottom_left_ref_pos))
    print('After homography:')
    print('\tAux pos: ' + str(matrix_mult(homography, aux_pos)))
    print('\tBottom left pos: ' + str(matrix_mult(homography, bottom_left_ref_pos)))
    print('\tPrinter at (0, 0)' + str(matrix_mult(homography, printer_positions[-1])))
    offset = -geom.Vec2(
            matrix_mult(homography, aux_pos).x,  # x from auxilliary marker
            matrix_mult(homography, bottom_left_ref_pos).y # y from bottom left ref
    )
    with open('calibration.json', 'w') as f:
        json.dump({
                'homography': numbers,
                'offset': tuple(offset)
            }, f)


if __name__ == '__main__':
    calc_and_save_calibration(*collect_points())
